class TicketNotifier < ApplicationMailer
  default from: 'kiranku marthella <kirankumarthella.dev@gmail.com>'

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.ticket_notifier.assigned.subject
  #
  def assigned(ticket)
    @ticket = ticket

    mail to: User.find_by(id: ticket.user_id)[:email], subject: 'New ticket assigned'
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.ticket_notifier.solved.subject
  #
  def solved
    @greeting = "Hi"

    mail to: ticket.email, subject: 'Your ticket have been solved'
  end
end
